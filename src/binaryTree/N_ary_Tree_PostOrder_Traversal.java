package binaryTree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class N_ary_Tree_PostOrder_Traversal {

    public List<Integer> postOrder(Node root) {
        List<Integer> result = new ArrayList<>();
        if (root == null) {
            return result;
        }
        Stack<Node> stack = new Stack<>();
        Node temp;

        stack.push(root);
        while (! stack.empty()) {
            temp = stack.pop();
            result.add(0, temp.val);
            int size = temp.children.size();
            for (int i = 0; i < size; i++) {
                stack.push(temp.children.get(i));
            }
        }
        return result;
    }
}
