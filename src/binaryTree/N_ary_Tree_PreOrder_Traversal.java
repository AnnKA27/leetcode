package binaryTree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class N_ary_Tree_PreOrder_Traversal {

    public List<Integer> preorder(Node root) {
        List<Integer> result = new ArrayList<>();

        if (root == null) {
            return result;
        }
        Stack<Node> stack = new Stack<>();
        Node temp;
        stack.push(root);
        while (! stack.isEmpty()) {
            temp = stack.pop();
            result.add(temp.val);
            int size = temp.children.size();
            for (int i = size - 1; i >= 0; i--) {
                stack.push(temp.children.get(i));
            }
        }
        return result;
    }

    public List<Integer> preOrderRecursive(Node root) {
        List<Integer> result = new ArrayList<>();

        if (root == null) {
            return result;
        }
        for (Node node : root.children) {
            preOrderRecursive(node);
        }
        return result;
    }
}
