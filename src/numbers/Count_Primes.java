package numbers;

/**
 * Count the number of prime numbers less than a non-negative number, n.
 * * Example:
 * Input: 10
 * Output: 4
 * Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
 */
public class Count_Primes {
    public static void main(String[] args) {
        System.out.println(countPrimes(10));
    }

    public static int countPrimes(int n) {
        int count = 0;
        for (int i = 1; i < n; i++) {
            int innerCount = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    innerCount++;
                }
            }
            if (innerCount == 2) {
                count++;
            }
        }
        return count;
    }

    public static int countPrimesSecond(int n) {
        boolean[] isPrime = new boolean[ n ];
        for (int i = 2; i < n; i++) {
            isPrime[ i ] = true;
        }
        for (int i = 2; i * i < n; i++) {
            if (! isPrime[ i ]) {
                continue;
            }
            for (int j = i * i; j < n; j += i) {
                isPrime[ j ] = false;
            }
        }
        int count = 0;
        for (int i = 2; i < n; i++) {
            if (isPrime[ i ]) {
                count++;
            }
        }
        return count;
    }
}

