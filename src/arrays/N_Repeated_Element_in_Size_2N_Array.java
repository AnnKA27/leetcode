package arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * In a array A of size 2N, there are N+1 unique elements, and exactly one of these elements is repeated N times.
 * Return the element repeated N times.
 * Example 1:
 * Input: [1,2,3,3]
 * Output: 3
 */

public class N_Repeated_Element_in_Size_2N_Array {
    public static void main(String[] args) {
        System.out.println(repeatedNTimes(new int[] { 1, 2, 3, 3 }));
    }

    public static int repeatedNTimes(int[] A) {
        Map<Integer, Integer> count = new HashMap<>();

        for (int i : A) {
            count.put(i, count.getOrDefault(i, 0) + 1);
        }

        for (int k : count.keySet()) {
            if (count.get(k) > 1) {
                return k;
            }
        }
        return 0;
    }
}
