package arrays;

import java.util.HashSet;
import java.util.Set;

/**
 * Given an array of integers, find if the array contains any duplicates.
 * Your function should return true if any value appears at least twice in the array,
 * and it should return false if every element is distinct.
 * Example 1:
 * Input: [1,2,3,1]
 * Output: true
 */
public class Contains_Duplicate {
    public static void main(String[] args) {

    }

    public boolean containsDuplicate(int[] nums) {
        Set<Integer> res = new HashSet<>();
        for (int i : nums) {
            res.add(i);
        }
        return nums.length != res.size();
    }

    public boolean containsDuplicate2(int[] nums) {
        Set<Integer> res = new HashSet<>();
        for (int i : nums) {
            if (! res.add(i)) {
                return true;
            }
        }
        return false;
    }
}
