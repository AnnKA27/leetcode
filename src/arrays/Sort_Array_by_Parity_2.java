package arrays;

import java.util.Arrays;

/**
 * Given an array A of non-negative integers, half of the integers in A are odd, and half of the integers are even.
 * Sort the array so that whenever A[i] is odd, i is odd; and whenever A[i] is even, i is even.
 * You may return any answer array that satisfies this condition.
 * Example 1:
 * Input: [4,2,5,7]
 * Output: [4,5,2,7]
 * Explanation: [4,7,2,5], [2,5,4,7], [2,7,4,5] would also have been accepted.
 */
public class Sort_Array_by_Parity_2 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(sortArrayByParityII(new int[] {5,2,3,4})));
    }

    public static int[] sortArrayByParityII(int[] array) {
        int[] result = new int[ array.length ];
        int iEven = 0;
        int iOdd = 1;
        for (int a : array) {
            if (a % 2 == 0) {
                result[ iEven ] = a;
                iEven += 2;
            } else {
                result[ iOdd ] = a;
                iOdd += 2;
            }
        }

        return result;
    }
}
