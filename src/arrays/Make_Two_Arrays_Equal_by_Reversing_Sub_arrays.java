package arrays;

/**
 * Given two integer arrays of equal length target and arr.
 * In one step, you can select any non-empty sub-array of arr and reverse it. You are allowed to make any number of steps.
 * Return True if you can make arr equal to target, or False otherwise.
 * Example 1:
 * Input: target = [1,2,3,4], arr = [2,4,1,3]
 * Output: true
 * Explanation: You can follow the next steps to convert arr to target:
 * 1- Reverse sub-array [2,4,1], arr becomes [1,4,2,3]
 * 2- Reverse sub-array [4,2], arr becomes [1,2,4,3]
 * 3- Reverse sub-array [4,3], arr becomes [1,2,3,4]
 * There are multiple ways to convert arr to target, this is not the only way to do so.
 */

public class Make_Two_Arrays_Equal_by_Reversing_Sub_arrays {
    public static void main(String[] args) {
        System.out.println(canBeEqual(new int[] {1,2,3,4}, new int[] {2,4,1,3}));
    }

    public static boolean canBeEqual(int[] target, int[] arr) {
        if (target.length != arr.length) {
            return false;
        }
        int[] temp = new int[ 1001 ];
        for (int i = 0; i < target.length; i++) {
           temp[target[i]]++;
           temp[arr[i]]--;

        }
        for (int i : temp) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }
}
