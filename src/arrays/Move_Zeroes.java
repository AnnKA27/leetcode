package arrays;

import java.util.Arrays;

/**
 * Given an array nums,write a function to move all 0's to the end of it
 * while maintaining the relative order of the non-zero elements.
 * Example:
 * Input: [0,1,0,3,12]
 * Output: [1,3,12,0,0]
 */
public class Move_Zeroes {
    public static void main(String[] args) {
        int[] test = new int[] { 1, 2, 0, 2, 0, 4 };
        moveZeroes(test);
        System.out.println(Arrays.toString(test));
    }

    public static void moveZeroes(int[] nums) {
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[ i ] != 0) {
                nums[ index ] = nums[ i ];
                index++;
            }
        }
        for (int i = index; i < nums.length; i++) {
            nums[ i ] = 0;
        }
    }
}

