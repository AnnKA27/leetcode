package arrays;

import java.util.ArrayList;
import java.util.List;

/**
 * A self-dividing number is a number that is divisible by every digit it contains.
 * For example, 128 is a self-dividing number because 128 % 1 == 0, 128 % 2 == 0, and 128 % 8 == 0.
 * Also, a self-dividing number is not allowed to contain the digit zero.
 * Given a lower and upper number bound, output a list of every possible self dividing number, including the bounds if possible.
 * Example 1:
 * Input:
 * left = 1, right = 22
 * Output: [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22]
 */

public class Self_Dividing_Numbers {
    public static void main(String[] args) {
        System.out.println(selfDividingNumbers(1, 22));
    }

    public static List<Integer> selfDividingNumbers(int left, int right) {
        List<Integer> result = new ArrayList<>();
        for (int i = left; i <= right; i++) {
            if (selfDividedNumber(i)) {
                result.add(i);
            }
        }
        return result;
    }

    private static boolean selfDividedNumber(int n) {
        int originNumber = n;
        while (n > 0) {
            if (n % 10 == 0) {
                return false;
            }
            int digit = n % 10;
            if (originNumber % digit != 0) {
                return false;
            }
            n = n / 10;
        }

        return true;
    }
}
