package arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Given two arrays, write a function to compute their intersection.
 * Example 1:
 * Input: nums1 = [1,2,2,1], nums2 = [2,2]
 * Output: [2,2]
 * Example 2:
 * Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
 * Output: [4,9]
 */
public class Intersection_of_two_arrays {
    public static void main(String[] args) {
        int[] num1 = new int[] { 1, 2, 2, 3, 8 };
        int[] num2 = new int[] { 2, 2, 3, 6, 7 };
        System.out.println(Arrays.toString(intersectSecond(num1, num2)));
    }

    public static int[] intersect(int[] nums1, int[] nums2) {
        List<Integer> result = new ArrayList<>();
        List<Integer> temp = new ArrayList<>();
        for (int num : nums1) {
            temp.add(num);
        }
        for (int num : nums2) {
            if (temp.contains(num)) {
                result.add(num);
            }
            temp.remove((Integer) num);
        }
        return result.stream().mapToInt(i -> i).toArray();
    }

    public static int[] intersectSecond(int[] nums1, int[] nums2) {
        Map<Integer, Integer> map = new HashMap<>();

        // Put elements into a map with count
        for (int i : nums1) {
            map.put(i, map.getOrDefault(i, 0) + 1);
        }

        // check commons
        ArrayList<Integer> arr = new ArrayList<>();
        for (int i : nums2) {
            if (map.getOrDefault(i, 0) == 0) {
                continue;
            }
            map.put(i, map.get(i) - 1);
            arr.add(i);
        }

        // Copy it to an int array and return
        int result[] = new int[ arr.size() ];
        int c = 0;
        for (int i : arr) {
            result[ c++ ] = i;
        }
        return result;
    }
}
