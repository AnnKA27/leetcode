package arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Given a non-empty array of integers, every element appears twice except for one. Find that single one.
 * Note:
 * Your algorithm should have a linear runtime complexity. Could you implement it without using extra memory?
 * Example 1:
 * Input: [2,2,1]
 * Output: 1
 * Example 2:
 * Input: [4,1,2,1,2]
 * Output: 4
 */
public class Single_Number {
    public static void main(String[] args) {

    }

    public static int singleNumber(int[] nums) {
        List<Integer> result = new ArrayList<>();
        for (int i : nums) {
            if (! result.contains(i)) {
                result.add(i);
            } else {
                result.remove(i);
            }
        }
        return result.get(0);
    }

    public static int singleNumberSecond(int[] nums) {
        Map<Integer, Integer> result = new HashMap<>();
        for (int i : nums) {
            result.put(i, result.getOrDefault(i, 0) + 1);
        }
        for (int i : nums) {
            if (result.get(i) == 1) {
                return i;
            }
        }
        return 0;
    }
}
