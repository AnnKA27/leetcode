package arrays;

import java.util.Arrays;

/**
 * Given an array of integers A sorted in non-decreasing order,
 * return an array of the squares of each number, also in sorted non-decreasing order.
 * Example 1:
 * Input: [-4,-1,0,3,10]
 * Output: [0,1,9,16,100]
 */
public class Squares_of_a_Sorted_Array {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(sortedSquares(new int[] { - 4, - 1, 0, 3, 10 })));
    }

    public static int[] sortedSquares(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            arr[ i ] *= arr[ i ];
        }
        Arrays.sort(arr);
        return arr;
    }
}
