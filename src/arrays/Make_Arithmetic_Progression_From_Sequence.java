package arrays;

import java.util.Arrays;

/**
 * Given an array of numbers arr.
 * A sequence of numbers is called an arithmetic progression if the difference between any two consecutive elements is the same.
 * Return true if the array can be rearranged to form an arithmetic progression, otherwise, return false.
 * Example 1:
 * Input: arr = [3,5,1]
 * Output: true
 * Explanation: We can reorder the elements as [1,3,5] or [5,3,1] with differences 2 and -2 respectively, between each consecutive elements.
 */
public class Make_Arithmetic_Progression_From_Sequence {
    public static void main(String[] args) {
        System.out.println(canMakeArithmeticProgression(new int[] { 18, 8, 0, 15, 4, - 3, 17, - 18, - 4, - 7, - 12, 5, - 16, 20 }));
    }

    public static boolean canMakeArithmeticProgression(int[] arr) {
        Arrays.sort(arr);
        int diff = arr[ 1 ] - arr[ 0 ];
        for (int i = 2; i < arr.length; i++) {
            if (arr[ i ] - arr[ i - 1 ] != diff) {
                return false;
            }
        }
        return true;
    }
}
