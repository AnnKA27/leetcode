package arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * Given an array of size n, find the majority element.
 * The majority element is the element that appears more than ⌊ n/2 ⌋ times.
 * You may assume that the array is non-empty and the majority element always exist in the array.
 * Example 1:
 * Input: [3,2,3]
 * Output: 3
 */
public class Majority_Element {
    public static void main(String[] args) {

    }

    public static int majorityElement(int[] nums) {
        Map<Integer, Integer> counts = new HashMap<>();
        for (int num : nums) {
            counts.put(num, counts.getOrDefault(num, 0) + 1);
            if (counts.get(num) > nums.length / 2) {
                return num;
            }
        }
        return 0;
    }

    public static int majorityElementSecond(int[] nums){
        int major = nums[0];
        int count = 1;
        for (int i = 1; i < nums.length; i++){
            if(major == nums[i]){
                count++;
            }
            else{
                count--;
            }
            if(count == 0){
                major = nums[i];
                count++;
            }
        }
        return major;
    }

}
