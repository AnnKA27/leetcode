package arrays;

import java.util.HashSet;
import java.util.Set;

/**
 * Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, find the one that is missing from the array.
 * Example 1:
 * Input: [3,0,1]
 * Output: 2
 * Example 2:
 * Input: [9,6,4,2,3,5,7,0,1]
 * Output: 8
 */
public class Missing_Number {
    public static void main(String[] args) {

    }

    public static int missingNumber(int[] nums) {
        Set<Integer> numSet = new HashSet<>();
        for (int num : nums) {
            numSet.add(num);
        }
        int expectedNumCount = nums.length + 1;
        for (int number = 0; number < expectedNumCount; number++) {
            if (! numSet.contains(number)) {
                return number;
            }
        }
        return - 1;
    }

    public static int missingNumberSecond(int[] nums) {
        //  по формуле арифметической прогрессии
        int expectedSum = nums.length * (nums.length + 1) / 2;
        int actualSum = 0;
        for (int num : nums) {
            actualSum += num;
        }
        return expectedSum - actualSum;
    }
}
