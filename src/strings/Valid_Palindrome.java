package strings;

/**
 * Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.
 * Note: For the purpose of this problem, we define empty string as valid palindrome.
 * Example 1:
 * Input: "A man, a plan, a canal: Panama"
 * Output: true
 */
public class Valid_Palindrome {
    public static void main(String[] args) {

    }

    public static boolean isPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return true;
        }
        String palindrome = s.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
        int startIndex = 0;
        int endIndex = palindrome.length() - 1;
        while (startIndex < endIndex) {
            if (palindrome.charAt(startIndex++) != palindrome.charAt(endIndex--)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isPalindromeSecond(String s) {
        if (s == null || s.length() == 0) {
            return true;
        }
        int left = 0;
        int right = s.length() - 1;
        String str = s.toLowerCase();
        while (left < right) {
            if (! Character.isLetterOrDigit(str.charAt(left))) {
                left++;
                continue;
            }
            if (! Character.isLetterOrDigit(str.charAt(right))) {
                right--;
                continue;
            }

            if (str.charAt(left) != str.charAt(right)) {
                return false;
            }

            left++;
            right--;
        }
        return true;
    }
}

