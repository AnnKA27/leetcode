package strings;

/**
 * Given an integer n, return a string with n characters such that each character in such string occurs an odd number of times.
 * The returned string must contain only lowercase English letters. If there are multiples valid strings, return any of them.
 * Example 1:
 * Input: n = 4
 * Output: "pppz"
 * Explanation: "pppz" is a valid string since the character 'p' occurs three times and the character 'z' occurs once.
 * Note that there are many other valid strings such as "ohhh" and "love".
 */

public class Odd_String_Characters {
    public static void main(String[] args) {
        System.out.println(generateTheString(6));
    }

    public static String generateTheString(int n) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < n - 1; i++) {
            result.append("a");
        }
        return n % 2 == 0 ? result.append("b").toString() : result.append("a").toString();
    }
}

