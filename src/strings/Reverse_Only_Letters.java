package strings;

/**
 * Given a string S, return the "reversed" string where all characters that are not a letter stay in the same place,
 * and all letters reverse their positions.
 * Example 1:
 * Input: "ab-cd"
 * Output: "dc-ba"
 */
public class Reverse_Only_Letters {
    public static void main(String[] args) {

    }

    public static String reverseOnlyLetters(String S) {
        char[] characters = S.toCharArray();
        int begin = 0;
        int end = S.length() - 1;

        while (begin < end) {
            while (begin < end && ! Character.isLetter(S.charAt(begin))) {
                begin++;
            }
            while (end > begin && ! Character.isLetter(S.charAt(end))) {
                end--;
            }

            char temp = characters[ begin ];
            characters[ begin++ ] = characters[ end ];
            characters[ end-- ] = temp;
        }

        return new String(characters);
    }
}
