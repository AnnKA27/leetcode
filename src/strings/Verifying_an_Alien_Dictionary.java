package strings;

/**
 * In an alien language, surprisingly they also use english lowercase letters,
 * but possibly in a different order. The order of the alphabet is some permutation of lowercase letters.
 * Given a sequence of words written in the alien language, and the order of the alphabet,
 * return true if and only if the given words are sorted lexicographicaly in this alien language.
 * Example 1:
 * Input: words = ["hello","leetcode"], order = "hlabcdefgijkmnopqrstuvwxyz"
 * Output: true
 * Explanation: As 'h' comes before 'l' in this language, then the sequence is sorted.
 */
public class Verifying_an_Alien_Dictionary {
    public static void main(String[] args) {
        isAlienSorted(null, "hlabcdefgijkmnopqrstuvwxyz");
    }

    public static boolean isAlienSorted(String[] words, String order) {
        int[] index = new int[ 26 ];
        for (int i = 0; i < order.length(); i++) {
            index[ order.charAt(i) - 'a' ] = i;
        }
        search:
        for (int i = 0; i < words.length - 1; i++) {
            String firstWord = words[ i ];
            String secondWord = words[ i + 1 ];

            for (int k = 0; k < Math.min(firstWord.length(), secondWord.length()); k++) {
                if (firstWord.charAt(k) != secondWord.charAt(k)) {
                    if (index[ firstWord.charAt(k) - 'a' ] > index[ secondWord.charAt(k) - 'a' ]) {
                        return false;
                    }
                    continue search;
                }
            }
            if (firstWord.length() > secondWord.length()) {
                return false;
            }
        }

        return true;
    }
}
