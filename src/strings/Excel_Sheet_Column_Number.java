package strings;

/**
 * Given a column title as appear in an Excel sheet, return its corresponding column number.
 * For example:
 * <p>
 * A -> 1
 * B -> 2
 * C -> 3
 * ...
 * Z -> 26
 * AA -> 27
 * AB -> 28
 * ...
 * Example 1:
 * Input: "A"
 * Output: 1
 * Example 2:
 * Input: "AB"
 * Output: 28
 */
public class Excel_Sheet_Column_Number {
    public static void main(String[] args) {

    }

    public static int titleToNumber(String s) {
        int total = 0;

        for (int i = 0; i < s.length(); i++) {
            total += (s.charAt(i) - 64) * Math.pow(26, s.length() - 1 - i);
        }
        return total;
    }
}
